alias reload!='. ~/.zshrc'
alias vim='/usr/local/bin/vim'
alias nvimrc='nvim ~/.config/nvim/init.vim'
