if test ! $(which spoof)
then
  sudo npm install spoof -g
fi

if test ! $(which nvm)
then
  echo "  Installing NVM for you."
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh | bash
fi

exit 0
